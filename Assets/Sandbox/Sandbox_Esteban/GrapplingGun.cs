﻿using UnityEngine;

public class GrapplingGun : MonoBehaviour
{
    [Header("Scripts:")]
    public GrappleRope grappleRope;
    [Header("Layer Settings:")]
    [SerializeField] private bool grappleToAll = false;
    [SerializeField] private int grappableLayerNumber = 9;

    public LayerMask IgnoreMe;

    [Header("Main Camera")]
    public Camera m_camera;

    [Header("Transform Refrences:")]
    public Transform gunHolder;
    public Transform gunPivot;
    public Transform firePoint;
    public Transform grappling;

    [Header("Rotation:")]
    [SerializeField] private bool rotateOverTime = true;
    [Range(0, 80)] [SerializeField] private float rotationSpeed = 4;

    [Header("Distance:")]
    [SerializeField] private bool hasMaxDistance = true;
    [SerializeField] private float maxDistance = 4;

    [Header("Launching")]
    [SerializeField] private bool launchToPoint = true;
    [SerializeField] private LaunchType Launch_Type = LaunchType.Transform_Launch;
    [Range(0, 5)] [SerializeField] private float launchSpeed = 5;

    [Header("No Launch To Point")]
    [SerializeField] private bool autoCongifureDistance = false;
    [SerializeField] private float targetDistance = 3;
    [SerializeField] private float targetFrequency = 3;


    private enum LaunchType
    {
        Transform_Launch,
        Physics_Launch,
    }

    [Header("Component Refrences:")]
    public SpringJoint2D m_springJoint2D;

    [HideInInspector] public Vector2 grapplePoint;
    [HideInInspector] public Vector2 DistanceVector;
    Vector2 Mouse_FirePoint_DistanceVector;

    public Rigidbody2D playerRb;

    public CharacterController2D playerController;

    public SpriteRenderer playerRenderer;


    private void Start()
    {
        grappleRope.enabled = false;
        m_springJoint2D.enabled = false;
    }

    private void Update()
    {
        Mouse_FirePoint_DistanceVector = m_camera.ScreenToWorldPoint(Input.mousePosition) - gunPivot.position;

        if (Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            SetGrapplePoint();
            playerController.enabled = false;
            playerRb.gravityScale = 1;

        }
        else if (Input.GetKey(KeyCode.Joystick1Button1))
        {
            if (grappleRope.enabled)
            {
                RotateGun(grapplePoint, false);
            }
            else
            {
                RotateGun(m_camera.ScreenToWorldPoint(Input.mousePosition), false);
            }

            if (launchToPoint && grappleRope.isGrappling)
            {
                if (Launch_Type == LaunchType.Transform_Launch)
                {
                    gunHolder.position = Vector3.Lerp(gunHolder.position, grapplePoint, Time.deltaTime * launchSpeed);
                }
            }

            if (playerRb.velocity.x > 0)
            {
                playerRenderer.flipX = false;
            }
            else
            {
                playerRenderer.flipX = true;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Joystick1Button1))
        {
            grappleRope.enabled = false;
            m_springJoint2D.enabled = false;
            playerController.enabled = true;
            playerRb.gravityScale = playerController.gravityForce;
        }
        else
        {
            RotateGun(m_camera.ScreenToWorldPoint(Input.mousePosition), true);
        }
    }

    public void GrapplinInit()
    {
        SetGrapplePoint();
        playerController.enabled = false;
        playerRb.gravityScale = 1;
    }

    public void GrapplinHold()
    {
        if (grappleRope.enabled)
        {
            RotateGun(grapplePoint, false);
        }
        else
        {
            RotateGun(m_camera.ScreenToWorldPoint(Input.mousePosition), false);
        }

        if (launchToPoint && grappleRope.isGrappling)
        {
            if (Launch_Type == LaunchType.Transform_Launch)
            {
                gunHolder.position = Vector3.Lerp(gunHolder.position, grapplePoint, Time.deltaTime * launchSpeed);
            }
        }

        if (playerRb.velocity.x > 0)
        {
            playerRenderer.flipX = false;
        }
        else
        {
            playerRenderer.flipX = true;
        }
    }

    public void GrapplinEnd()
    {
        grappleRope.enabled = false;
        m_springJoint2D.enabled = false;
        playerController.enabled = true;
        playerRb.gravityScale = playerController.gravityForce;
    }

    void RotateGun(Vector3 lookPoint, bool allowRotationOverTime)
    {
        if (playerController.ifPcTest)
        {
            lookPoint = new Vector2(playerController.horizontal, playerController.vertical);
        }
        else
        {
            lookPoint = playerController._joystick.Direction;
        }

        Vector3 distanceVector = lookPoint - gunPivot.position;

        float angle = Mathf.Atan2(distanceVector.y, distanceVector.x) * Mathf.Rad2Deg;
        if (rotateOverTime && allowRotationOverTime)
        {
            Quaternion startRotation = gunPivot.rotation;
            gunPivot.rotation = Quaternion.Lerp(startRotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * rotationSpeed);

        }
        else
        {
            if (!playerRenderer.flipX)
            {
                float angleBidule = Mathf.Atan2(lookPoint.x, lookPoint.y) * Mathf.Rad2Deg;
                gunPivot.rotation = Quaternion.AngleAxis(angleBidule - 90f, Vector3.back);
                gunPivot.localScale = new Vector3(1, 1, 1);
            }
            else
            {
                float angleBidule = Mathf.Atan2(lookPoint.x, lookPoint.y) * Mathf.Rad2Deg;
                gunPivot.rotation = Quaternion.AngleAxis(-angleBidule - 90f, Vector3.forward);
                gunPivot.localScale = new Vector3(-1, 1, 1);
            }
        }
            

    }

    void SetGrapplePoint()
    {
        Vector3 shootDirection = firePoint.position - grappling.position;
        if (Physics2D.Raycast(firePoint.position, shootDirection, 1000f, IgnoreMe))
        {
            RaycastHit2D _hit = Physics2D.Raycast(firePoint.position, shootDirection, 1000f, IgnoreMe);
            if ((_hit.transform.gameObject.layer == grappableLayerNumber || grappleToAll) && ((Vector2.Distance(_hit.point, firePoint.position) <= maxDistance) || !hasMaxDistance))
            {
                grapplePoint = _hit.point;
                DistanceVector = grapplePoint - (Vector2)gunPivot.position;
                grappleRope.enabled = true;
            }
        }
    }

    public void Grapple()
    {

        if (!launchToPoint && !autoCongifureDistance)
        {
            m_springJoint2D.distance = targetDistance;
            m_springJoint2D.frequency = targetFrequency;
        }

        if (!launchToPoint)
        {
            if (autoCongifureDistance)
            {
                m_springJoint2D.autoConfigureDistance = true;
                m_springJoint2D.frequency = 0;
            }
            m_springJoint2D.connectedAnchor = grapplePoint;
            m_springJoint2D.enabled = true;
        }

        else
        {
            if (Launch_Type == LaunchType.Transform_Launch)
            {
                playerRb.gravityScale = 0;
                playerRb.velocity = Vector2.zero;
            }
            if (Launch_Type == LaunchType.Physics_Launch)
            {
                m_springJoint2D.connectedAnchor = grapplePoint;
                m_springJoint2D.distance = 0;
                m_springJoint2D.frequency = launchSpeed;
                m_springJoint2D.enabled = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (hasMaxDistance)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(firePoint.position, maxDistance);
        }
    }

}
