using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script contenant les informations de chaque plan�te
/// </summary>
public class PlaneteInfo : MonoBehaviour
{
    public string namePlanete;
    public int indexPlanete;
    
    public int difficulty;

    public bool oxygene;

    public bool planeteCompleted;




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
