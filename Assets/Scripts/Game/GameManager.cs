using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

/// <summary>
/// Script gérant le bon déroulement du jeu 
/// </summary>
public class GameManager : MonoBehaviour
{
    /*public Animation fuseeAtterrissage;
    public Animation fuseeDecollage;*/

    public Animator fuseeAnimator;


    public Transform fuseeSpawnPoint;
    public GameObject goFusee;
    public GameObject Player;
    public SpriteRenderer playerSP;

    public CinemachineVirtualCamera vc1;

    bool once = true;

    // Start is called before the first frame update
    void Start()
    {
        fuseeAnimator.GetComponent<Animator>();
        vc1.GetComponent<CinemachineVirtualCamera>();

        fuseeAnimator.SetBool("Arrivee", true);
        fuseeAnimator.SetBool("StartDecollage", false);
    }

    // Update is called once per frame
    void Update()
    {

        Debug.Log(fuseeAnimator);
        
        if(fuseeAnimator.GetCurrentAnimatorClipInfo(0).Length >= 1 && once)
        {
            StartCoroutine(Wait());

            once = false;
        }
    }

    IEnumerator Wait()
    {
        fuseeAnimator.SetBool("Arrivee", false);

        yield return new WaitForSeconds(2f);


        Debug.Log("Drop player");
        Player.transform.position = fuseeSpawnPoint.position;
        playerSP.color = new Color(255, 255, 255, 255);

        //vc1.m_Lens.Orthographic = true;
        vc1.m_Lens.OrthographicSize = 8f;
        vc1.Follow = Player.transform;

        
    }

    public void EnterFuseeGameManager()
    {
        Debug.Log("dans le GM");

        fuseeAnimator.SetBool("StartDecollage", true);
        fuseeAnimator.SetBool("Arrivee", false);
        
        vc1.Follow = goFusee.transform;

    }

}
