using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    [Header("Paramétrage du joueur")]
    [SerializeField] private float speed;
    [SerializeField] private float jumpingPower;
    [SerializeField] public float gravityForce;
    [SerializeField] private LayerMask groundLayer;

    [HideInInspector] public float horizontal;
    [HideInInspector] public float vertical;
    private bool isFacingRight = true;

    [Header("Fonctionnels")]
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    

    public FixedJoystick _joystick;

    public SpriteRenderer spriteRenderer;

    public Animator animator;

    public bool landingGrappling;

    [Header("Controle suplémentaire")]
    public BoxCollider2D bxClldFusee;
    public GameObject goBntEntrer;
    private GameManager gm;

    [Header("TEST DEV")]
    public bool ifPcTest;



    private void Start()
    {
        rb.gravityScale = gravityForce;
        //gm.GetComponent<GameManager>();

        gm = FindObjectOfType<GameManager>();
    }

    private void OnEnable()
    {
        landingGrappling = true;
    }

    void Update()
    {
        if (ifPcTest)
        {
            horizontal = Input.GetAxisRaw("Horizontal");
            vertical = Input.GetAxisRaw("Vertical");
            _joystick.GetComponent<FixedJoystick>().handle.anchoredPosition = new Vector2(horizontal, vertical) * _joystick.GetComponent<FixedJoystick>().background.sizeDelta / 2;
        }
        else
        {
            horizontal = _joystick.Horizontal;
        }

        if (!landingGrappling)
        {
            animator.SetBool("IsWalking", Mathf.Abs(horizontal) > 0f);
        }      

        if (Input.GetButtonDown("Jump") && IsGrounded())
        {
            Jump();
        }

        if(landingGrappling && IsGrounded())
        {
            landingGrappling = false;
        }

        Flip();

    }

    private void FixedUpdate()
    {
        if (!landingGrappling)
        {
            rb.velocity = new Vector2(horizontal * speed * Time.deltaTime, rb.velocity.y);
        }       
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.1f, groundLayer);
    }

    public void Jump()
    {
        rb.AddForce(Vector2.up * jumpingPower, ForceMode2D.Impulse);
    }

    private void Flip()
    {
        if (isFacingRight && horizontal < 0f || !isFacingRight && horizontal > 0f)
        {
            isFacingRight = !isFacingRight;

            spriteRenderer.flipX = !isFacingRight;
        }
    }

    public void EnterFusee()
    {
        gm.EnterFuseeGameManager();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
      
        if (collision.gameObject.tag == "Fusee")
        {
            Debug.Log("enter la la collision");

            goBntEntrer.SetActive(true);


        }

        if (collision.gameObject.tag == "Grappin" & GetComponentInChildren<GrappleRope>().isGrappling)
        {
            Debug.Log("grap");
            Jump();
            GetComponentInChildren<GrapplingGun>().GrapplinEnd();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Fusee")
        {
            goBntEntrer.SetActive(false);
        }
    }


}