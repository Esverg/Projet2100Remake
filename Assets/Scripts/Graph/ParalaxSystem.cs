using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxSystem : MonoBehaviour
{
    public Camera cam;
    public Transform playerTrsf;

    Vector2 startPos;
    float startZ;

    Vector2 travel => (Vector2)cam.transform.position - startPos;

    float distanceFromSubject => transform.position.z - playerTrsf.position.z;
    float clippingPlane => (cam.transform.position.z + (distanceFromSubject > 0 ? cam.farClipPlane : cam.nearClipPlane));
    
    float paralaxFactor => Mathf.Abs(distanceFromSubject) / clippingPlane;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
        startZ = transform.position.z;

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 newPos = startPos + travel * paralaxFactor;
        transform.position = new Vector3(newPos.x, newPos.y, startZ);
    }
}
